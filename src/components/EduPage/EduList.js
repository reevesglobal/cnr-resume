import React from 'react';
import EduCardFront from './EduCardFront.js';
import EduCardBack from './EduCardBack.js';

class EduList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            toggled: {
                masters: false,
                honours: false, 
                undergrad: false
            }
        }
    }

    toggleCard = id => {
        this.setState(prevState => {
            return {
                toggled: {
                    ...prevState.toggled,
                    [id]: !prevState.toggled[id]
                }
            }
        })
    }

    render() {
        return (
            <div id='education-page'>
                <h4>Tertiary Education Details</h4>
                <div>
                    {
                        this.props.eduObject.map(card => this.state.toggled[card.id]
                            ? (
                                <EduCardBack 
                                    key={card.id} 
                                    {...card}
                                    toggleCard={this.toggleCard.bind(this, card.id)}
                                />
                            ) 
                            : (
                                <EduCardFront 
                                    key={card.id} 
                                    {...card}
                                    toggleCard={this.toggleCard.bind(this, card.id)}
                                />
                            )
                        )
                    }
                </div>
            </div>
        )
    }
}

export default EduList;