import React from 'react';

const EduCardBack = ({ 
    toggleCard, 
    project, 
    details, 
    names 
}) => {
    return (
        <div className="edu-card-back">
            <h3>{project}</h3>
            <hr></hr>
            <p>{details}</p>
            <hr></hr>
            <h5>{names}</h5>
            <button 
                className="flip-btn" 
                onClick={toggleCard}
            >less details</button>
        </div>
    )
}

export default EduCardBack;