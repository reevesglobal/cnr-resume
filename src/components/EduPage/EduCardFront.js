import React from 'react';

const EduCardFront = ({ 
    toggleCard, 
    title, 
    description, 
    location, 
    duration 
}) => {
    return (
        <div className="edu-card">
            <h3>{title}</h3>
            <hr></hr>
            <p>{description}</p>
            <hr></hr>
            <h5>{location}</h5>
            <h5>{duration}</h5>
            <button 
                className="flip-btn"    
                onClick={toggleCard} 
            >more details</button>
        </div>
    )
};

export default EduCardFront;