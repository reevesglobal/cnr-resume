export const eduObject = [
    {
        id: "masters", 
        title: "Master's Degree (M.Sc.) in the Physiological Sciences",
        description: "Thesis-based degree specialising in skeletal muscle's fibrotic scar response to injury, and the role of Nitric Oxide in the post-injury healing process.",
        location: "Stellenbosch University",
        duration: "January '14 - December '16 || 3 years",
        project: "Thesis Title: Exogenous Nitric Oxide - A Beneficial Role in Skeletal Muscle Regeneration.",
        details: "A Nitric Oxide donating therapy, known as Molsidomine, was given to rodents after a skeletal muscle impact injury. Molsidomine treatment reduced the extent of fibrotic scar formation in regenerated muscle tissue, compared to Placebo-treated subjects. This is beneficial, in that scar tissue interferes with whole muscle function and is thus an unwanted by-product of muscle injury. Additionally, at the Indian Ocean Rim Muscle Conference, held in Stellenbosch in 2016, I was awarded best research visual presentation.",
        names: "Supervisors: Prof KH Myburgh & Prof C Smith"
    },
    {
        id: "honours", 
        title: "Honour's Degree (B.Sc. Hons.) in the Physiological Sciences",
        description: "A higher education degree specialising in the major aspects of human physiology, with a thesis focusing on skeletal muscle's functional capacity after fatigue-injury.",
        location: "Stellenbosch University",
        duration: "January '12 - December '12 || 1 year",
        project: "Thesis Title: The Effect of Plyometric Jumping Activity on Muscle Function in Healthy Males.",
        details: "Healthy, male volunteers were recruited to participate in a muscle-damaging plyometric jumping intervention. Muscle functional capacity was evaluated before and after intervention with an isometric force-testing chair, and blood markers of muscle damage were measured. Plyometric jumping decreased muscle force output, which was correlated with elevated levels of circulating muscle damage markers in the blood.",
        names: "Supervisor: Prof KH Myburgh"
    },
    {
        id: "undergrad", 
        title: "Bachelor's Degree (B.Sc.) in Sports & Exercise Science",
        description: "A degree focused on the biological, biomechanical and physiological processes behind human sports and exercise.",
        location: "Stellenbosch University",
        duration: "January '08 - December '11 || 4 years",
        project: "",
        details: "Theory subjects included: Exercise Physiology, Sports Injuries, Biomechanics, Anatomy and Sport Psychology. The degree included a large practical element, through which I earned level 1 coaching qualifications for Field Hockey, Swimming, Tennis and a number of other sports. I also specialised in sports & exercise training for the disabled, which was both fascinating and rewarding.",
        names: ""
    }
]