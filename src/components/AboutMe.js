import React from "react";

const AboutMeCard = () => {
  return (
    <div className="about-me-card">
      <h3>Who am I?</h3>
      <hr />
      <p>I am a Physiologist, a Web Developer, a Data Scientist and a Teacher.</p>
      <p>I am also a sports & exercise fanatic, who is mildly obsessed with personal health.</p>
      <p>My personality assessment suggests that I am heavily concerned with 'people mastery'. Who knows what that means.</p>
      <p>I love everything about the natural world, and I am fascinated by the world that we've built, too.</p>
      <hr />
      <p>I put this website together with React Native, much Googling and heaps of patience.</p>
      <p>Please have a look through the pages, and, if neccessary, contact me.</p>
      <p>I hope you enjoy!</p>
      <hr />
    </div>
  );
};

class AboutMe extends React.Component {
  render() {
    return (
      <div>
        <AboutMeCard />
      </div>
    );
  }
}

export default AboutMe;
