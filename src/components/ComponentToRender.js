import React from 'react';

const ComponentToRender = ({ renderComponent }) => {
    return (
        <div id='rendered-component'>
            {renderComponent()}
        </div>
    )
}

export default ComponentToRender;