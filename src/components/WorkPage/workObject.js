export const workObject = [
    {
        id: "job1", 
        title: "English Teacher",
        description: "Teaching English as a second language for children between the ages of 4yrs and 15yrs.",
        location: "Apax English - Vietnam",
        duration: "April '18 - current || ~ 1 year",
        names: "",
        details: "http://apaxenglish.com/"
    },
    {
        id: "job2", 
        title: "Testing & Validation Scientist",
        description: "In this role, I tested and validated biometric wearable-devices and health-tracking algorithms. My responsibilities included the execution of testing protocols (e.g VO2 Max tests), collecting and handling biometric data as well as plotting and analysing this data (using Python). I also wrote scientific research reports and literature reviews as a part of the Biology Team.",
        location: "HealthQ Technologies",
        duration: "June '16 - January '18 || 20 months",
        names: "",
        details: "https://healthq.co/"
    },
    {
        id: "job3", 
        title: "Scientific Consultant",
        description: "I worked on a number of research projects in aid to Prof KH Myburgh. This included managing and coordinating studies on nutritional sports supplements in collaboration with the South African Institute for Drug Free Sport, conducting physical performance tests on athletes as well as helping and mentoring new postgraduate students with their research.",
        location: "Stellenbosch University",
        duration: "January '16 - May '16 || 6 months",
        names: "",
        details: "http://www0.sun.ac.za/physiologicalsci/eng/index.php"
    }, 
    {
        id: "job4", 
        title: "Practical Lesson Demonstrator",
        description: "I prepared and facilitated Physiology practical classes for undergraduate students during my postgraduate studies.",
        location: "Stellenbosch University",
        duration: "January '14 - November '15 || ~ 2 years",
        names: "",
        details: "http://www0.sun.ac.za/physiologicalsci/eng/index.php"
    },
    {
        id: "job5", 
        title: "Library Research Assistant",
        description: "I assisted fellow Masters, PhD, and Postdoctoral researchers with Microsoft Word and Excel related issues, referencing their research as well as searching for articles relevant to their respective work.",
        location: "Stellenbosch University",
        duration: "January '14 - December '14 || 1 year",
        names: "",
        details: "https://library.sun.ac.za/en-za/Research/rc/Pages/default.aspx"
    },
    {
        id: "job6", 
        title: "Ice Hockey Coach",
        description: "I was a head coach in the Western Cape Ice Hockey Association, coaching players from age 6 up to the age of 25. I was the assistant coach of the under 18 South African National Team for two years. I was also the South African representative coach at the IIHF’s Coaching Development and Education Program in Vierumäki, Finland.",
        location: "South African Ice Hockey Association",
        duration: "January '13 - March '16 || 3 years",
        names: "",
        details: "http://www.wpicehockey.co.za/"
    }
]