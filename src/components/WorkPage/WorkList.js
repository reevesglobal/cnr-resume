import React from 'react';
import WorkCardFront from './WorkCardFront.js';
import WorkCardBack from './WorkCardBack.js';

class WorkList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            toggled: {
                job1: false,
                job2: false,
                job3: false,
                job4: false,
                job5: false,
                job6: false
            }
        }
    }

    toggleCard = id => {
        this.setState(prevState => {
            return {
                toggled: {
                    ...prevState.toggled,
                    [id]: !prevState.toggled[id]
                }
            }
        })
    }

    render() {
        return (
            <div id='work-page'>
                <h4>Work Experience</h4>
                <div>
                    {
                        this.props.workObject.map(card => this.state.toggled[card.id] 
                            ? (
                                <WorkCardBack 
                                    key={card.id}
                                    {...card}
                                    toggleCard={this.toggleCard.bind(this, card.id)}
                                />
                            )
                            : (
                                <WorkCardFront
                                    key={card.id}
                                    {...card}
                                    toggleCard={this.toggleCard.bind(this, card.id)}
                                />
                            )
                        )
                    }
                </div>
            </div>
        )
    }
}

export default WorkList;