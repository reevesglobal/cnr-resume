import React from 'react';

const WorkCardBack = ({ 
    toggleCard, 
    description,
    details 
}) => {
    return (
        <div className="work-card-back">
            <hr></hr>
            <p>{description}</p>
            <p><a href={details} target="_blank" rel="noopener noreferrer">{details}</a></p>
            <hr></hr>
            <button 
                className="flip-btn" 
                onClick={toggleCard}
            >less details</button>
        </div>
    )
}

export default WorkCardBack;