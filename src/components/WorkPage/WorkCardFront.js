import React from 'react';

const WorkCardFront = ({ 
    toggleCard, 
    title, 
    location, 
    duration 
}) => {
    return (
        <div className="work-card">
            <h3>{title}</h3>
            <hr></hr>
            <h5>{location}</h5>
            <h5>{duration}</h5>
            <hr></hr>
            <button 
                className="flip-btn"    
                onClick={toggleCard} 
            >more details</button>
        </div>
    )
};

export default WorkCardFront;