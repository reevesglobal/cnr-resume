import React from "react";

const Nav = ({
  triggerHome,
  triggerAboutMe,
  triggerEducation,
  triggerWork
}) => {
  return (
    <nav id="nav-bar">
      <p
        className="nav-item"
        onClick={() => triggerHome()}
      >
        Home
      </p>
      <p
        className="nav-item"
        onClick={() => triggerAboutMe()}
      >
        About
      </p>
      <p
        className="nav-item"
        onClick={() => triggerEducation()}
      >
        Yoga
      </p>
      <p
        className="nav-item"
        onClick={() => triggerWork()}
      >
        Experience
      </p>
      <p
        className="nav-item"
        onClick={() => triggerEducation()}
      >
        Education
      </p>
    </nav>
  );
};

export default Nav;
