import React from "react";

const MyName = () => {
    return (
        <div id="my-name">
            <h2 style={{fontSize: "1000%"}}>Christopher Nicholas Reeves</h2>
            <h3>aspiring polymath</h3>
        </div>
    );
};

export default MyName;